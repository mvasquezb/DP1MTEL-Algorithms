package com.dp1mtel.algo.tdfunction;

import com.dp1mtel.algo.model.Problem;

public class DefaultTDFunctionFactory implements TDFunctionFactory {
    @Override
    public String getName() {
        return "DEFAULT";
    }

    @Override
    public TDFunction createTDFunction(Problem problem) {
        final double[][] distanceMatrix = problem.distanceMatrix();
        double closingTime = problem.getDepot().getClosingTime();

        return (from, to, startTime) -> {
            if (startTime > closingTime || startTime < 0) {
                return Double.POSITIVE_INFINITY;
            }

            return distanceMatrix[from][to];
        };
    }
}
