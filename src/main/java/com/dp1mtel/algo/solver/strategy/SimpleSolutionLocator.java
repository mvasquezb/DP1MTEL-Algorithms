package com.dp1mtel.algo.solver.strategy;

import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.solver.strategy.SolutionLocator;
import com.dp1mtel.algo.solver.strategy.optimizer.inter_route.RelocateMethod;
import com.dp1mtel.algo.solver.strategy.optimizer.intra_route.TwoOptMethod;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleSolutionLocator implements SolutionLocator {
    public static int NEIGHBOURHOOD_SIZE = 20;

    private final TwoOptMethod intraRoute;
    private final RelocateMethod interRoute;
    private final Problem problem;

    public SimpleSolutionLocator(Problem problem) {
        this.problem = problem;
        this.interRoute = new RelocateMethod(problem);
        this.intraRoute = new TwoOptMethod(problem);
    }

    @Override
    public List<Solution> findNeighbours(Solution solution) {
        // Intra-route improvements
        intraRoute.twoOpt(solution);
        // Inter-route improvements
        List<Solution> newSolutions = interRoute.neighbourhood(solution, NEIGHBOURHOOD_SIZE);
        newSolutions = cleanSolutions(newSolutions);
        return newSolutions;
    }

    private List<Solution> cleanSolutions(List<Solution> solutions) {
        Collections.shuffle(solutions);
        return solutions.subList(0, NEIGHBOURHOOD_SIZE);
    }

    @Override
    public Solution findBestNeighbour(List<Solution> candidates, List<Solution> tabuSolutions) {
        return candidates.stream()
                .filter((sol) -> !tabuSolutions.contains(sol))
                .min((sol1, sol2) -> (int) (sol1.getCost() - sol2.getCost()))
                .get();
    }
}
