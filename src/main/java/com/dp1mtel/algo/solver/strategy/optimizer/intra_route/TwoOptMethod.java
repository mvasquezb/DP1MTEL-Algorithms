package com.dp1mtel.algo.solver.strategy.optimizer.intra_route;

import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.Route;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class TwoOptMethod {
    public static double GAIN_DELTA = 0.000_001;

    private final Problem problem;
    private final TDFunction tdFunction;

    public TwoOptMethod(Problem problem) {
        this.problem = problem;
        this.tdFunction = problem.getTDFunction();
    }

    public boolean twoOpt(Solution solution) {
        boolean improves = false;
        for (Route r : solution.getRoutes()) {
            improves = improves || this.twoOpt(r);
        }
        return improves;
    }

    public boolean twoOpt(Route route) {
        double bestGain = Double.NEGATIVE_INFINITY;
        int[] bestSwap = new int[2];

        for (int i = 1; i < route.size() - 2; i++) {
            for (int k = i + 1; k < route.size() - 1; k++) {
                double gain = calculateGain(route, i, k);

                if (gain > bestGain) {
                    bestSwap[0] = i; bestSwap[1] = k;
                    bestGain = gain;
                }
            }
        }

        if (bestGain > 0) {
            route.swap(bestSwap[0], bestSwap[1]);
            return true;
        }
        return false;
    }

    public List<Solution> neighbourhood(Solution solution, int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Invalid neighbourhood size");
        }
        List<Solution> neighbours = new ArrayList<>();
        List<Route> routes = solution.getRoutes();

        // Random node swap
        Collections.shuffle(routes);
        for (int r = 0; r < size; r++) {
            for (Route route : routes) {
                int i = ThreadLocalRandom.current().nextInt(1, route.size() - 1);
                int k = ThreadLocalRandom.current().nextInt(i + 1, route.size());

                route.swap(i, k);
            }
        }
        return neighbours;
    }

    private double calculateGain(Route route, int i, int j) {
        int n1 = route.get(i - 1).getNumber();
        int n1Next = route.get(i).getNumber();
        int n2 = route.get(j).getNumber();
        int n2Next = route.get(j + 1).getNumber();

        double[][] costMatrix = problem.distanceMatrix();
        double cost = costMatrix[n1][n1Next] + costMatrix[n2][n2Next] -
                costMatrix[n1][n2] - costMatrix[n1Next][n2Next];

        return cost > GAIN_DELTA ? cost : 0;
    }
}
