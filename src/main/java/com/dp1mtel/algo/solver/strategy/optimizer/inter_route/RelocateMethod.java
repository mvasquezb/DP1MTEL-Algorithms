package com.dp1mtel.algo.solver.strategy.optimizer.inter_route;

import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.Route;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class RelocateMethod {

    public static double GAIN_DELTA = 0.000_001;

    private final Problem problem;
    private final TDFunction tdFunction;

    public RelocateMethod(Problem problem) {
        this.problem = problem;
        this.tdFunction = problem.getTDFunction();
    }

    /**
     * Attempt moving customer from route B to route A while respecting constraints
     * Moving a customer into the same route such that Route(A) == Route(B) is allowed
     *
     * @param a
     * @param b
     * @return true if improvement occurs, false otherwise
     */
    public double relocate(Route a, Route b) {
        return relocate(Collections.singletonList(a), b);
    }

    /**
     * Attempt moving customer from route B to any other route while respecting constraints
     *
     * @param aList
     * @param b
     * @return true if improvement occurs, false otherwise
     */
    public double relocate(List<Route> aList, Route b) {
        return relocate(aList, b);
    }

    /**
     * Attempt moving a customer from a route to any other route while respecting constraints
     *
     * @param solution
     * @return true if improvement occurs, false otherwise
     */
    public double relocate(Solution solution) {
        return relocate(solution.getRoutes());
    }

    private double relocate(List<Route> aList, Route srcRoute, boolean best) {
        double bestGain = Double.NEGATIVE_INFINITY;

        Route bestRoute = null;
        int[] bestRelocate = new int[2];

        // Attempt move for every route in aList
        for (Route destRoute : aList) {
            // Skip depots
            int srcNodes = srcRoute.getRoute().size();
            int destNodes = destRoute.getRoute().size();
            for (int i = 1; i < srcNodes - 1; i++) {
                for (int j = 0; j <  destNodes - 1; j++) {
                    double gain = calculateGain(destRoute, srcRoute, j, i);
                    if (gain > bestGain) {
                        // Update `best` values
                        bestGain = gain;
                        bestRoute = destRoute;
                        bestRelocate[0] = i; bestRelocate[1] = j;
                    }
                }
            }
        }

        if (bestGain > 0 && bestRoute != null) {
            move(bestRoute, srcRoute, bestRelocate[1], bestRelocate[0]);
            return bestGain;
        }
        return -1;
    }

    private double relocate(List<Route> aList) {
        double bestGain = Double.NEGATIVE_INFINITY;

        Route bestSrcRoute = null;
        Route bestDestRoute = null;
        int[] bestRelocate = new int[2];

        for (Route srcRoute : aList) {
            int srcNodes = srcRoute.getRoute().size();
            for (Route destRoute : aList) {
                int destNodes = destRoute.getRoute().size();
                for (int i = 1; i < srcNodes - 1; i++) {
                    for (int j = 1; j < destNodes - 1; j++) {
                        double gain = calculateGain(destRoute, srcRoute, j, i);
                        if (gain > bestGain) {
                            bestGain = gain;
                            bestSrcRoute = srcRoute;
                            bestDestRoute = destRoute;
                            bestRelocate[0] = i; bestRelocate[1] = j;
                        }
                    }
                }
            }
        }
        if (bestGain > 0 && bestDestRoute != null && bestSrcRoute != null) {
            move(bestDestRoute, bestSrcRoute, bestRelocate[1], bestRelocate[0]);
            return bestGain;
        }
        return -1;
    }

    public List<Solution> neighbourhood(Solution solution, int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Invalid neighbourhood size");
        }

        List<Route> routes = solution.getRoutes();
        List<Solution> neighbours = new ArrayList<>();

        Collections.shuffle(routes);
        for (int sInd = 0; sInd < routes.size(); sInd++) {
            Route srcRoute = routes.get(sInd);
            int srcNodes = srcRoute.getRoute().size();
            for (int dInd = 0; dInd < routes.size(); dInd++) {
                Route destRoute = routes.get(dInd);
                int destNodes = destRoute.getRoute().size();

                if (neighbours.size() >= size) {
                    return neighbours;
                }

                // Random nodes to swap
                boolean solIsValid = false;
                Solution neighbour = null;
                while (!solIsValid && neighbour == null) {
                    int i = ThreadLocalRandom.current().nextInt(1, srcNodes);
                    int j = ThreadLocalRandom.current().nextInt(1, destNodes);
                    neighbour = buildNewSolution(solution, dInd, sInd, j, i);
                    solIsValid = neighbour.isValid();
                }
                neighbours.add(neighbour);
            }
        }
        return neighbours;
    }

    private Solution buildNewSolution(Solution initial, int destIndex, int srcIndex, int j, int i) {
        Solution sol = new Solution(initial);
        move(sol.getRoutes().get(destIndex), sol.getRoutes().get(srcIndex), j, i);
        return sol;
    }

    private double calculateGain(Route destRoute, Route srcRoute, int j, int i) {
        int n1 = srcRoute.get(i).getNumber();
        int n1Prev = srcRoute.get(i - 1).getNumber();
        int n1Next = srcRoute.get(i + 1).getNumber();
        int n2 = destRoute.get(j).getNumber();
        int n2Next = destRoute.get(j + 1).getNumber();

        double[][] distanceMatrix = problem.distanceMatrix();
        // Avoid invalid routes (i.e. 0 -> 0)
        double n1PrevToNextCost = n1Prev == n2Next ? 0 : distanceMatrix[n1Prev][n1Next];
        double n2NextCost = n2 == n2Next ? 0 : distanceMatrix[n2][n2Next];

        double cost = n2NextCost + distanceMatrix[n1Prev][n1] + distanceMatrix[n1][n1Next] -
                distanceMatrix[n2][n1] - distanceMatrix[n1][n2Next] - n1PrevToNextCost;

        return cost > GAIN_DELTA ? cost : 0;
    }

    private void move(Route routeA, Route routeB, int j, int i) {
        if (routeA == routeB) {
            routeA.moveNode(i, j);
        } else {
            routeA.add(j + 1, routeB.get(i));
            routeB.removeNode(i);
        }
    }
}
